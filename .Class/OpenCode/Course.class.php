<?php
/**
 * @package OpenCode
 * @ProjectName: Umbrella
 * @version 1.0.0
 * @link https://bitbucket.org/AlvesDouglaz/OpenCode The Umbrella Bitbucket project
 * @author Douglas Alves <alves.douglaz@gmail.com>
 * @copyright (c) 2014, Douglas Alves <alves.douglaz@gmail.com>
 * @Date 06/02/2015 
 * @note Esta classe, juntamente com o package, podem ser utilizadas por qualquer
 * pessoa, porem não pode ser alterada em qualquer condição
 * 
 * ******************************************************************************
 * <b>Course</b>
 * Model responsavel por criar, editar, excluir, cirar modulos, aulas, etc dos cursos
 * ******************************************************************************
 */

namespace OpenCode {
    class Course extends \Umbrella\Database
    {

        private $CourseId;
        private $CourseName;
        private $CourseDescription;
        private $CourseOwner;
        private $CoursePlan;
        private $CourseCertify;
        private $CourseStatus;
        private $CourseData;
        private $CourseResult;
        private $CourseError;

        /** TABLES * */
        const TableCourse = "course";

        /** FIELDS * */
        const Course_Name = "course_name";
        const Course_Description = "course_description";
        const Course_Owner = "course_owner";
        const Course_Plan = "course_plan";
        const Course_Certify = "course_certify";
        const Course_Status = "course_status";


        public function __construct()
        {
            parent::__construct( \DB_NAME, \DB_USER, \DB_PASS, \DB_NAME );
        }


        /** Getters * */
        public function getCourseId()
        {
            return $this->CourseId;
        }


        public function getLastCourseId()
        {
            $this->setIdCourseLastInsert();
            return $this->CourseId;
        }


        public function getCourseResult()
        {
            return $this->CourseResult;
        }


        public function getCourse( $Query = null, $ParseString = null )
        {
            parent::QRSelect( self::TableCourse, "{$Query}", "{$ParseString}" );
            $this->CourseResult = parent::getResult();
            return $this->getCourseResult();
        }


        /**
         * @param array $Data = Array assosiativo com as informações do curso
         */
        public function newCourse( array $Data )
        {
            $this->CourseData = $Data;
            $this->insertCourse();
            $this->setIdCourseLastInsert();
        }


        /**
         * Faz a inserção dos dados do curso na tabela
         */
        private function insertCourse()
        {
            $Datas = $this->CourseData;
            parent::QRInsert( self::TableCourse, $Datas );
        }


        private function setIdCourseLastInsert()
        {
            $Query = "ORDER BY id DESC LIMIT 1";
            $GetID = parent::QRSelect( self::TableCourse, $Query );
            $this->CourseId = parent::getResult()[ 0 ][ 'id' ];
        }


    }
}