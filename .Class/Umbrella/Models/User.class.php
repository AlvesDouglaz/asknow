<?php
/**
 * @package Umbrella
 * @ProjectName: Umbrella
 * @version 2.0
 * @link https://bitbucket.org/AlvesDouglaz/umbrella The Umbrella Bitbucket project
 * @author Douglas Alves <alves.douglaz@gmail.com>
 * @copyright (c) 2014, Douglas Alves <alves.douglaz@gmail.com>
 * @Date 14/11/2014 
 * @note Esta classe, juntamente com o package, podem ser utilizadas por qualquer
 * pessoa, porem não pode ser alterada em qualquer condição sem previa do autor!
 * 
 * ******************************************************************************
 * <b>Database</b>
 * Responsavel pela validacao, criacao, autenticação e manipulacao de usuarios
 * ******************************************************************************
 */

namespace Umbrella\Models {
    class User
    {

        private $Level;
        private $Email;
        private $Senha;
        private $Error;
        private $Result;
        private $Token;


        /** @param type $Level = Informar o level minimo que o usuario deve ter */
        function __construct( $Level )
        {
            $this->Level = ( int ) $Level;
        }


        public function getResult()
        {
            return $this->Result;
        }


        public function getError()
        {
            return $this->Error;
        }


        public function getCurrentUser()
        {
            if ( $this->checkLogin() ) :
                return $_SESSION[ 'userLogin' ];
            else:
                return false;
            endif;
        }


        /**  @param array $Datas = Array retornado do formulário com o email e senha do usuário */
        public function Logar( array $Datas )
        {
            $this->Email = ( string ) strip_tags( trim( $Datas[ 'email' ] ) );
            $this->Senha = ( string ) strip_tags( trim( sha1( md5( $Datas[ 'senha' ] ) ) ) );
            $this->actionLogin();
        }


        /** Faz o logout do usuario matando as sessoes */
        public function Logout()
        {
            $this->Result = FALSE;
            if ( isset( $_SESSION[ 'userLogin' ] ) ) :
                unset( $_SESSION[ 'userLogin' ] );
            endif;
        }


        /**
         * <b>checkLogin: </b>Verifica se o usuario esta logado
         * @global $DB Instancia com o banco de dados
         * @return boolean retorna TRUE se o usuario estiver logado ou FALSE 
         * se nao estiver logado
         */
        public function checkLogin()
        {
            global $DB;
            if ( empty( $_SESSION[ 'userLogin' ] ) || $_SESSION[ 'userLogin' ][ 'user_level' ] < $this->Level ):
                $this->Result = FALSE;
                if ( isset( $_SESSION[ 'userLogin' ] ) ) {
                    unset( $_SESSION[ 'userLogin' ] );
                }
                return FALSE;
            else :
                $authUser = $DB;
                $authUser->QRSelect( "user", "WHERE user_email = :e AND user_password = :p AND user_level = :l AND user_token = :t", "e={$_SESSION[ 'userLogin' ][ 'user_email' ]}&p={$_SESSION[ 'userLogin' ][ 'user_password' ]}&l={$_SESSION[ 'userLogin' ][ 'user_level' ]}&t={$_SESSION[ 'userLogin' ][ 'user_token' ]}" );
                if ( $authUser->getResult() ) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            endif;
        }


        /**
         * <b>actionLogin: </b>Faz as verificações antes de logar
         */
        private function actionLogin()
        {
            if ( !$this->Email || !$this->Senha || \Umbrella\Helper::CheckEmail( $this->Email ) ) :
                $this->Error = "Login ou senha não pode ser vazio";
                $this->Result = FALSE;
                if ( isset( $_SESSION[ 'userLogin' ] ) ) :
                    unset( $_SESSION[ 'userLogin' ] );
                endif;

            elseif ( !$this->getUser() ) :
                $this->Error = "Os dados informados estão incorretos";
                $this->Result = FALSE;
                if ( isset( $_SESSION[ 'userLogin' ] ) ) :
                    unset( $_SESSION[ 'userLogin' ] );
                endif;

            elseif ( $this->Result[ 'user_level' ] < $this->Level ) :
                $this->Result = FALSE;
                $this->Error = "Você não tem premissão para acessar essa área";
            else:
                $this->setAuthentication();
            endif;
        }


        /**
         * <b>getUser: </b>Verifica se o email e a senha existem no banco de dados
         * @global $DB = Instancia com o banco de dados
         * @return boolean = Retorna TRUE ou FALSE e joga o resultado dentro do
         * atribulto Result
         */
        private function getUser()
        {
            global $DB;
            $this->setToken();
            $getUser = $DB;
            $getUser->QRSelect( "user", "WHERE user_email = :e AND user_password = :p", "e={$this->Email}&p={$this->Senha}" );
            if ( $getUser->getResult() ) :
                $this->Result = $getUser->getResult()[ 0 ];
                return true;
            else:
                return false;
            endif;
        }


        /**
         * <b>setToken: </b>Cria um token aleatorio para validar um unico usuário por vez
         * @global type $DB = Instancia com o banco de dados 
         */
        private function setToken()
        {
            global $DB;
            $this->Token = sha1( date( 'Y-m-d h:i:s' ) . $this->Email . 'UMBR3LL4' . $this->Senha );
            $setToken = $DB;
            $setToken->QRUpdate( "user", ["user_token" => "{$this->Token}" ], "WHERE user_email = :e AND user_password = :p", "e={$this->Email}&p={$this->Senha}" );
        }


        /** <b>setAuthentication: </b>Cria a sessao do usuario */
        private function setAuthentication()
        {
            if ( !session_id() ) :
                session_start();
            endif;
            $_SESSION[ 'userLogin' ] = $this->Result;
            $this->Result = true;
        }


        public function recoverPassword( $Email )
        {
            $this->Email = $Email;
            if ( $this->checkEmail() ):
                if ( $this->Mailer() ) {
                    \Umbrella\Alert::PHPErro( "Recuperação enviada", E_USER_NOTICE, null );
                } else {
                    \Umbrella\Alert::PHPErro( "Nao enviado: $this->Error", E_USER_ERROR, null );
                }
            endif;
        }


        private function checkEmail()
        {
            global $DB;
            $checkEmail = $DB;
            $checkEmail->QRSelect( "user", "WHERE user_email = :e", "e={$this->Email}" );
            if ( $checkEmail->getResult() ) :
                return TRUE;
            else:
                return FALSE;
            endif;
        }


        private function Mailer()
        {
            $Mailer = new \Umbrella\Mailer();
            /* ====================================
              CONFIGURAÇÕES DO SERVIDOR DE ENVIO
              =================================== */
            $Mailer->IsSMTP();
            $Mailer->IsHTML( true );
            $Mailer->CharSet = 'UTF-8';
            $Mailer->SMTPDebug = 0;
            $Mailer->SMTPAuth = true;
            $Mailer->SMTPSecure = "ssl";
            $Mailer->Host = \MAIL_HOST;
            $Mailer->Port = \MAIL_PORT;
            $Mailer->Username = \MAIL_USER;
            $Mailer->Password = \MAIL_PASS;

            $Mailer->Subject = '<Recover Password> - Umbrella';
            $Mailer->MsgHTML( 'Para resetar sua senha siga este link: <a href="http://recoverpassword.com/?id=019273809810298301982asd" target="_blank">http://recoverpassword.com/?id=019273809810298301982asd</a>' );
            $Mailer->AddAddress( $this->Email );

            if ( $Mailer->Send() ):
                $this->Result = TRUE;
                return TRUE;
            else:
                $this->Error = $Mailer->ErrorInfo;
                $this->Result = FALSE;
                return FALSE;
            endif;
        }


    }
}