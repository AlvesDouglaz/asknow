<?php
$getexe = filter_input( INPUT_GET, 'exe', FILTER_DEFAULT );
$getexe = str_replace( '\\', DIRECTORY_SEPARATOR, $getexe );
$_ = DIRECTORY_SEPARATOR;
//QUERY STRING
if ( !empty( $getexe ) ):
    $includepatch = BASEPATCH . "${_}painel${_}controller${_}" . strip_tags( trim( $getexe ) . '.tpl.php' );
    $includeindex = BASEPATCH . "${_}painel${_}controller${_}" . strip_tags( trim( $getexe ) );
else:
    $includepatch = BASEPATCH . "${_}painel${_}controller${_}dashboard.tpl.php";
endif;

if ( file_exists( $includepatch ) ):
    require_once($includepatch);
elseif ( is_dir( $includeindex ) ):
    require_once($includeindex."${_}index.php");
else:
    echo "<div class=\"content notfound\">";
    Umbrella\Alert::PHPErro( "<b>Erro ao incluir controller:</b> /{$includepatch}", E_USER_ERROR );
    echo "</div>";
endif;