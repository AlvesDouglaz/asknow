-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.6.16 - MySQL Community Server (GPL)
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para formulario
CREATE DATABASE IF NOT EXISTS `formulario` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `formulario`;


-- Copiando estrutura para tabela formulario.form
CREATE TABLE IF NOT EXISTS `form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `form_name` varchar(255) NOT NULL,
  `form_url` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela formulario.form: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `form` DISABLE KEYS */;
INSERT INTO `form` (`id`, `user_id`, `form_name`, `form_url`) VALUES
	(1, 0, 'Pesquisa Teste', '0');
/*!40000 ALTER TABLE `form` ENABLE KEYS */;


-- Copiando estrutura para tabela formulario.form_fields
CREATE TABLE IF NOT EXISTS `form_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `formField_name` varchar(255) NOT NULL,
  `formField_slug` varchar(255) NOT NULL,
  `formField_type` varchar(255) NOT NULL,
  `formField_defaultValue` varchar(255) DEFAULT NULL,
  `formField_parent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FormID` (`form_id`),
  CONSTRAINT `FormID` FOREIGN KEY (`form_id`) REFERENCES `form` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela formulario.form_fields: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `form_fields` DISABLE KEYS */;
INSERT INTO `form_fields` (`id`, `form_id`, `formField_name`, `formField_slug`, `formField_type`, `formField_defaultValue`, `formField_parent`) VALUES
	(2, 1, 'Field do tipo Select', 'field-do-tipo-select', 'select', 'valor 01, valor 02, Valor 03, Valor 04', NULL),
	(3, 1, 'Field do tipo Radio', 'field-do-tipo-radio', 'radio', 'valor 01, valor 02, Valor 03, Valor 04', NULL),
	(4, 1, 'Field do tipo Texto', 'field-do-tipo-texto', 'text', '', NULL),
	(5, 1, 'Field do tipo Area de Texto', 'field-do-tipo-area-de-texto', 'textarea', '', NULL),
	(6, 1, 'Field do tipo Checkbox', 'field-do-tipo-checkbox', 'checkbox', 'Valor 01, Valor 02, Valor 03, Valor 04', NULL);
/*!40000 ALTER TABLE `form_fields` ENABLE KEYS */;


-- Copiando estrutura para tabela formulario.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL DEFAULT '0',
  `user_email` varchar(255) NOT NULL DEFAULT '0',
  `user_password` varchar(255) NOT NULL DEFAULT '0',
  `user_level` varchar(100) NOT NULL DEFAULT '0',
  `user_token` varchar(100) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela formulario.user: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `user_name`, `user_email`, `user_password`, `user_level`, `user_token`) VALUES
	(4, 'Admin', 'admin', '036d0ef7567a20b5a4ad24a354ea4a945ddab676', '100', '3fecaec2ce19830d6c29a6ab1633d686da2b7d78');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
