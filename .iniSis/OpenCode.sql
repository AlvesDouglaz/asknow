-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.6.16 - MySQL Community Server (GPL)
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela open_code.course
CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(255) NOT NULL,
  `course_plan` int(3) NOT NULL,
  `course_certify` int(3) NOT NULL DEFAULT '0',
  `course_description` varchar(255) NOT NULL,
  `course_owner` int(11) NOT NULL,
  `course_status` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Owner` (`course_owner`),
  CONSTRAINT `Owner` FOREIGN KEY (`course_owner`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela open_code.course: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
INSERT INTO `course` (`id`, `course_name`, `course_plan`, `course_certify`, `course_description`, `course_owner`, `course_status`) VALUES
	(28, 'Curso de Teste', 1, 1, 'Este curso Ã© apenas um teste', 1, 2),
	(29, 'Curso de PHP', 0, 0, 'Aprenda MVC e PHP orientado a objetos', 1, 1);
/*!40000 ALTER TABLE `course` ENABLE KEYS */;


-- Copiando estrutura para tabela open_code.module
CREATE TABLE IF NOT EXISTS `module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(100) NOT NULL,
  `module_description` varchar(255) NOT NULL,
  `module_courseId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `CourseID` (`module_courseId`),
  CONSTRAINT `CourseID` FOREIGN KEY (`module_courseId`) REFERENCES `course` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela open_code.module: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `module` DISABLE KEYS */;
INSERT INTO `module` (`id`, `module_name`, `module_description`, `module_courseId`) VALUES
	(3, 'Orientacao a Objetos', 'Aprenda o padrao de programacao Orientado a Objetos', 29);
/*!40000 ALTER TABLE `module` ENABLE KEYS */;


-- Copiando estrutura para tabela open_code.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) NOT NULL DEFAULT '0',
  `user_email` varchar(255) NOT NULL DEFAULT '0',
  `user_password` varchar(255) NOT NULL DEFAULT '0',
  `user_level` varchar(100) NOT NULL DEFAULT '0',
  `user_token` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela open_code.user: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `user_name`, `user_email`, `user_password`, `user_level`, `user_token`) VALUES
	(1, 'Admin', 'admin', '036d0ef7567a20b5a4ad24a354ea4a945ddab676', '100', 'c3672424482995e64ab62ef2dfb8c8ebb9a6fabf');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
