$(document).ready(function() {
    //Esconde menu esquerdo do painel
    $('.ctr-menu').click(function() {
        var cls = $('aside').attr('data-view');
        if (cls == 'show') {
            $('aside').attr('data-view', 'hide');
            $('aside').hide('fast');
            $('main, .nav-opt').attr("style", "width: 100%;");
        } else if (cls == 'hide') {
            $('aside').attr('data-view', 'show');
            $('aside').show('fast');
            $('main, .nav-opt').attr("style", "");
        }
    });

    $("#userLogin").click(function() {
        var checado = $('#lembrar').is(':checked');
        if (checado) {
            if (typeof (Storage) != "undefined") {
                
                var usermail = $('#loginEmail').val();
                var usersenha = $('#loginSenha').val();
                
                localStorage.setItem("mail", usermail);
                localStorage.setItem("pass", usersenha);
            } else {
                //document.getElementById("result").innerHTML = "Sorry, your browser does not support Web Storage...";
                alert('Não foi possivel lembrar seu usuario e senha. Seu browser não dá suporte para esta tecnologia');
            }
        } 
    });
    $('#loginEmail').val(localStorage.getItem("mail"));
    $('#loginSenha').val(localStorage.getItem("pass"));
});