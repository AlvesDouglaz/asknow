<div class="navegation-links">
    <ul class="clearfix">
        <li>
            <i  class="icon-home4"></i> 
            <a href="#"> Dashboard</a>
            <i class="icon-chevron-right2"></i>
        </li>
        <li class="active">
            Certificados
        </li>
    </ul>
</div>
<div class="painel content">
    <section class="element">
        <div class="title">
            Certificados
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>#id</th>
                    <th>Aluno</th>
                    <th>Curso</th>
                    <th>Turma</th>
                    <th>Status</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>#id/Hash#</td>
                    <td>
                        <a href="#ID#" data-fileOpen="controller/certify/editCertify" class="newElement" title="Editar">#Aluno#</a>
                    </td>
                    <td>#Curso#</td>
                    <td>#Turma#</td>
                    <td>#Status#</td>
                    <td>
                        <a href="#" class="icon-trash2 action" title="Deletar"> </a>
                        <a href="#" data-fileOpen="controller/certify/editCertify" class="newElement icon-edit action" title="Editar"> </a>
                        <a href="#" class="icon-chart action" title="Ver Informações"> </a>
                        <a href="#" class="icon-folder2 action" title="Fechar"> </a>
                    </td>
                </tr>
            </tbody>
        </table>

    </section>
</div>