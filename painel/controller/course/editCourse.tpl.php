<?php 
require '../../../.iniSis/iniSys.php'; 
$elementId = $_GET['IdContent'];

$Course = new OpenCode\Course;
$CorrentCourse = $Course->getCourse( "WHERE id=:id", "id={$elementId}" )[0]; ?>
<section class="element a" style="display: none;">
    <div class="title">
        <i class="icon-close pull-right close-box"></i> 
        Criar novo curso
    </div>
    <form action="#" method="#">
        <table class="disp-i pull-left">
            <tr>
                <td>
                    <label class="medium">
                        <input type="text" value="ID: #<?php echo"{$CorrentCourse['id']}"?>" disabled/>
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label>
                        <span>Título do curso</span>
                        <input type="text" value="<?php echo"{$CorrentCourse['course_name']}"?>"/>
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label>
                        <span>Descrição</span>
                        <div class="textarea-group marg-b-0">
                            <textarea class="textarea no-resize"><?php echo"{$CorrentCourse['course_description']}"?></textarea>
                        </div>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <label>
                        <span>Status</span><br />
                        <div class="select-group marg-b-0">
                            <div></div>
                            <select name="">
                                <option value="2" <?php echo($CorrentCourse['course_status'] == 2) ? 'selected' : null; ?>>Aberto</option>
                                <option value="1" <?php echo($CorrentCourse['course_status'] == 1) ? 'selected' : null; ?>>Aguardando</option>
                                <option value="0" <?php echo($CorrentCourse['course_status'] == 0) ? 'selected' : null; ?>>Fechado</option>
                            </select>
                        </div>
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="wrap-buttons marg-t-20">
                        <button><i  class="icon-disk2"></i> Salvar</button>
                        <button class="newElement" data-fileOpen="controller/course/modules"><i  class="icon-edit"></i> Editar módulos</button>
                    </div>
                </td>
            </tr>
        </table>
        <table class="disp-i pull-left marg-l-20">
            <tr>
                <td>
                    <label for="">
                        <span style="width:100%;display: inline-block;">Plano do curso</span><br />
                        <div class="radio-group  marg-b-0">  
                            <input id="pago" type="radio" value="1" name="plan" <?php echo($CorrentCourse['course_plan'] == 1) ? 'checked' : null; ?> /> 
                            <label for="pago">Pago</label>

                            <input id="gratis" type="radio" value="0" name="plan" <?php echo($CorrentCourse['course_plan'] == 0) ? 'checked' : null; ?> />  
                            <label for="gratis">Gratuito</label>  
                        </div>
                    </label>
                </td>
                <td>
                    <label for="">
                        <span style="width:100%;display: inline-block;"><small>Esse curso dá direito a certificado?</small></span><br />
                        <div class="checkbox-group marg-b-0">  
                            <input id="chk1" type="checkbox" <?php echo($CorrentCourse['course_certify'] == 1) ? 'checked' : null; ?> /> 
                            <label for="chk1">Sim</label>
                        </div>
                    </label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span>Módulos</span>
                    <table class="table">
                        <tbody class="disp-b">
                            <tr class="disp-b">
                                <td>
                                    <a href="#">Introdução ao PHP</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</section>