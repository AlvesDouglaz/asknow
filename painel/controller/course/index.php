<?php
$User = new Umbrella\Models\User( 100 );
$CurrentUser = $User->getCurrentUser();

$Course = new OpenCode\Course;
$CourseCorrentUser = $Course->getCourse( "WHERE course_owner=:owner", "owner={$CurrentUser[ 'id' ]}" ); ?>

<div class="navegation-links">
    <ul class="clearfix">
        <li>
            <i  class="icon-home4"></i> 
            <a href="#"> Dashboard</a>
            <i class="icon-chevron-right2"></i>
        </li>
        <li class="active">
            Cursos
        </li>
    </ul>
</div>
<div class="painel content">
    <section class="element">
        <div class="title">
            Cursos
        </div>
        <div class="wrap-buttons">
            <button class="newElementBottom" data-fileOpen="controller/course/newCourse"><i  class="icon-plus3"></i> Criar novo curso</button>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>#id</th>
                    <th>Título</th>
                    <th>status</th>
                    <th>Alunos</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ( $CourseCorrentUser as $key ) : ?>
                    <tr>
                        <td><?php echo$key['id']?></td>
                        <td>
                            <a href="<?php echo$key['id']?>" data-fileOpen="controller/course/editCourse" class="newElement" title="Editar"><?php echo$key['course_name']?></a>
                        </td>
                        <td><?php echo$key['course_status']?></td>
                        <td>#Inscritos#</td>
                        <td>
                            <a href="#" class="icon-trash2 action" title="Deletar"> </a>
                            <a href="<?php echo$key['id']?>" data-fileOpen="controller/course/editCourse" class="newElement icon-edit action" title="Editar"> </a>
                            <a href="#" class="icon-chart action" title="Ver estatisticas"> </a>
                            <a href="#" class="icon-folder2 action" title="Fechar"> </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </section>
</div>