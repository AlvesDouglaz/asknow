<?php require '../../../.iniSis/iniSys.php'; ?>
<section class="element a" style="display: none;">
    <div class="title">
        <i class="icon-close pull-right close-box"></i> Módulos
    </div>
    <div class="wrap-buttons">
        <button class="newElementBottom" data-fileOpen="controller/course/newModule"><i  class="icon-plus3"></i> Adicionar módulo</button>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>Módulo</th>
                <th>Descrição</th>
                <th>Ação</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <a href="#">Introdução ao PHP</a>
                </td>
                <td>
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    </p>
                </td>
                <td class="action">
                    <a href="#" class="icon-trash2 action" title="Deletar"> </a>
                    <a href="#" class="icon-edit action" title="Editar"> </a>
                    <a href="#" class="icon-chart action" title="Ver estatisticas"> </a>
                    <a href="#" class="icon-folder2 action" title="Fechar"> </a>
                </td>
            </tr>

        </tbody>
    </table>
</section>