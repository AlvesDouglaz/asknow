<?php require '../../../.iniSis/iniSys.php'; ?>
<section class="element element-bottom a" style="display: none;">
    <div class="title">
        <i class="icon-close pull-right close-box"></i>
        Criar novo curso
    </div>

    <form action="#" method="#">
        <div class="medium pull-left">
            <span>Título do curso</span>
            <input type="text" />

            <span>Descrição do curso</span>
            <div class="textarea-group">
                <textarea class="textarea"></textarea>
            </div>
            <div class="wrap-buttons pull-right">
                <button onclick="return false"><i  class="icon-plus3"></i> Criar</button>
            </div>
        </div>
    </form>
</section>