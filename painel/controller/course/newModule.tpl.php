<?php require '../../../.iniSis/iniSys.php'; ?>
<section class="element element-bottom a" style="display: none;">
    <div class="title">
        <i class="icon-close pull-right close-box"></i>
        Adicionar Módulo
    </div>

    <form action="#" method="#">
        <label>
            <span>Título do módulo</span>
            <input type="text" />
        </label>
        <label class="medium">
            <span>Descrição do curso</span>
            <div class="textarea-group">
                <textarea class="textarea"></textarea>
            </div>
            <div class="wrap-buttons">
                <button onclick="return false"><i  class="icon-plus3"></i> Adicionar</button>
            </div>
        </label>
    </form>
</section>