<div class="navegation-links">
    <ul class="clearfix">
        <li>
            <i  class="icon-home4"></i> 
            <a href="#"> Dashboard</a>
            <i class="icon-chevron-right2"></i>
        </li>
        <li class="active">
            Turmas
        </li>
    </ul>
</div>
<div class="painel content">
    <section class="element">
        <div class="title">
            Turmas
        </div>
        <div class="wrap-buttons">
            <button class="newElementBottom" data-fileOpen="controller/team/newTeam"><i  class="icon-plus3"></i> Criar nova turma</button>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>#id</th>
                    <th>Curso</th>
                    <th>status</th>
                    <th>Alunos</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>#ID_Turma#</td>
                    <td>
                        <a href="#ID_Turma#" data-fileOpen="controller/course/editCourse" class="newElement" title="Editar"></a>
                    </td>
                    <td>#Turma_Status#</td>
                    <td>#Inscritos#</td>
                    <td>
                        <a href="#" class="icon-trash2 action" title="Deletar"> </a>
                        <a href="#ID_Turma#" data-fileOpen="controller/team/editTeam" class="newElement icon-edit action" title="Editar"> </a>
                        <a href="#" class="icon-chart action" title="Ver estatisticas"> </a>
                        <a href="#" class="icon-folder2 action" title="Fechar"> </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </section>
</div>