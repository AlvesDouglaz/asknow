<?php require '../../../.iniSis/iniSys.php'; ?>
<section class="element element-bottom a" style="display: none;">
    <div class="title">
        <i class="icon-close pull-right close-box"></i>
        Criar nova turma
    </div>

    <form action="#" method="#">

        <table>
            <tr>
                <td>
                    <div class="select-group">
                        <span>Atrelar turma ao curso:</span><br />
                        <select id="selectId">
                            <option value="#ID_do_curso#">#Nome_do_curso#</option>
                        </select>
                        <label for="selectId"></label>
                    </div>
                </td>
                <td class="_3col">
                    <div class="input-text-group"> 
                        <span for="txt">Limite de alunos: </span><br />
                        <input type="number" id="txt" />
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <label>
                        <div class="wrap-buttons">
                            <button onclick="return false"><i  class="icon-plus3"></i> Criar</button>
                        </div>
                    </label>
                </td>
            </tr>
        </table>
        
    </form>
</section>