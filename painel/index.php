<?php
require_once ('../.iniSis/iniSys.php');

$Login = new Umbrella\Models\User( 1 );
$logoff = filter_input( INPUT_GET, 'logout', FILTER_VALIDATE_BOOLEAN );

//Executa logoff
if ( !empty( $logoff ) ) {
    $Login->logout();
    header( 'Location: ' . BASEURL . '?logout=true' );
    die;
}

//Checa o login
if ( !$Login->checkLogin() ) {
    unset( $_SESSION[ 'userLogin' ] );
    header( 'Location: ' . BASEURL . '?restrito=true' );
    die;
} else {
    $UserCurrent = $Login->getCurrentUser();
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <link rel="stylesheet" href="css/main.css"/>
        <link rel="stylesheet" href="css/scrollbar.css"/>

        <title>OpenConde | <?php echo$UserCurrent['user_name']?></title>

        <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="js/scrollbar.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

    </head>

    <body>
        <aside data-view="show">
            <!--<a href="#" class="logo">
                <img src="imagens/logoumbrella.png" />
            </a>-->
            <div class="user-info">
                <img src="http://placehold.it/90x90" height="90" width="90" />
                <span class="user-name">Olá, <?php echo$UserCurrent['user_name']?></span>
                <ul class="user-actions clearfix">
                    <li>
                        <div class="wrap">
                            <i class="icon-user5"></i>
                            <p>perfíl</p>
                        </div>
                    </li>
                    <li>
                        <div class="wrap">
                            <i class="icon-cog3"></i>
                            <p>setting</p>
                        </div>
                    </li>
                    <li>
                        <div class="wrap">
                            <a href="?logout=true">
                                <i class="icon-logout"></i>
                                <p>logout</p>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
            <nav>
                <ul>
                    <?php $GetPage = filter_input( INPUT_GET, 'exe', FILTER_DEFAULT ); ?>

                    <li <?php echo (!isset( $GetPage ) ) ? 'class="active"' : null; ?>>
                        <i class="arrow"> </i>
                        <a href="index.php">
                            <i class="icon-dashboard"></i>
                            Dashboard
                        </a>
                    </li>
                    <li <?php echo ( $GetPage == 'course' ) ? 'class="active"' : null; ?>>
                        <i class="arrow"> </i>
                        <a href="?exe=course">
                            <i class="icon-question"></i>
                            Cursos
                        </a>
                    </li>
                    <li <?php echo ( $GetPage == 'team' ) ? 'class="active"' : null; ?>>
                        <i class="arrow"> </i>
                        <a href="?exe=team">
                            <i class="icon-pencil4"></i>
                            Turmas
                        </a>
                    </li>
                    <li <?php echo ( $GetPage == 'certify' ) ? 'class="active"' : null; ?>>
                        <i class="arrow"> </i>
                        <a href="?exe=certify">
                            <i class="icon-graduation"></i>
                            Certificados
                        </a>
                    </li>
                    <li <?php echo ( $GetPage == 'calendar' ) ? 'class="active"' : null; ?>>
                        <i class="arrow"> </i>
                        <a href="?exe=calendar">
                            <i class="icon-calendar-o"></i>
                            Calendário
                        </a>
                    </li>
                    <li <?php echo ( $GetPage == 'documents' ) ? 'class="active"' : null; ?>>
                        <i class="arrow"> </i>
                        <a href="?exe=documents">
                            <i class="icon-document2"></i>
                            Documentos
                        </a>
                    </li>
                    <li <?php echo ( $GetPage == 'groups' ) ? 'class="active"' : null; ?>>
                        <i class="arrow"> </i>
                        <a href="?exe=groups">
                            <i class="icon-group"></i>
                            Grupos
                        </a>
                    </li>
                    <li <?php echo ( $GetPage == 'forum' ) ? 'class="active"' : null; ?>>
                        <i class="arrow"> </i>
                        <a href="?exe=forum">
                            <i class="icon-comments-o"></i>
                            Fórum
                        </a>
                    </li>
                    <li <?php echo ( $GetPage == 'trash' ) ? 'class="active"' : null; ?>>
                        <i class="arrow"> </i>
                        <a href="?exe=trash">
                            <i class="icon-trash-o"></i>
                            Lixeira
                        </a>
                    </li>
                </ul>
            </nav>
        </aside>
        <main>
            <div class="nav-opt">
                <ul class="pull-left">
                    <li> <button class="icon-th-menu ctr-menu"></button> </li>
                    <li>
                        <input type="text" placeholder="Procurar" />
                        <button class="icon-search"></button> 
                    </li>
                </ul>
                <ul class="pull-right">
                    <li>
                        <span>10</span>
                        <a href="" class="icon-bell3"></a>
                    </li>
                    <li>
                        <span>205</span>
                        <a href="" class="icon-chart-pie"></a>
                    </li>
                </ul>
            </div>
            <div class="container">
                <!----------------------------------------------------------------------------->

                <?php require_once('../.Inc/FrontController.inc.php'); ?>

                <!----------------------------------------------------------------------------->
            </div>
        </main>
    </body>
</html>